package com.lirons.brewery.cities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.lirons.brewery.R;
import com.lirons.brewery.base.BaseActivity;
import com.lirons.brewery.breweries.BreweryActivity;
import com.lirons.brewery.data.cities.CityInner;
import com.lirons.brewery.databinding.CitiesActivityBinding;
import com.lirons.brewery.utiles.Constants;

import java.util.List;

import javax.inject.Inject;

public class CitiesActivity extends BaseActivity implements CitiesContract.View, CitiesAdapter.ICityClicked {

    @Inject
    public CitiesPresenter presenter;
    @Inject
    CitiesAdapter adapter;

    private CitiesActivityBinding binder;

    private SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binder = DataBindingUtil.setContentView(this, R.layout.cities_activity);
        binder.citiesList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binder.citiesList.setAdapter(adapter);

        setToolBarView();
        setTitle(getString(R.string.title_activity_cities));
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.setClickeListener(this);
        presenter.takeView(this);
        presenter.loadCities();
    }



    @Override
    protected void onPause() {
        super.onPause();
        if (searchView !=null && !searchView.isIconified()) {
            searchView.setIconified(true);
        }
        presenter.dropView();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showCities(List<CityInner> cities) {
        adapter.refreshList(cities);
    }

    @Override
    public void showLoadingCitiesError(String error) {

    }


    @Override
    public void onCityClicked(CityInner city) {
        Intent openMap = new Intent(this, BreweryActivity.class);
        openMap.putExtra(Constants.CITY_LAT, city.getLat());
        openMap.putExtra(Constants.CITY_LNG, city.getLng());
        openMap.putExtra(Constants.CITY_NAME, city.getCity());

        startActivity(openMap);
    }


    /********************************************************************************
     * menu item methods
     ********************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem search = menu.findItem(R.id.action_search);
        searchView = (SearchView) search.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                handleQueryTextChange(query);
                return false;
            }
        });
        return true;
    }

    private void handleQueryTextChange(String query) {

        if (searchView != null && !searchView.isIconified()) {
            if (!TextUtils.isEmpty(query)) {
                presenter.startAllQueryDataBase(query);

            } else {
                presenter.loadCities();
            }
        }
    }
}

