package com.lirons.brewery.cities;

import com.lirons.brewery.di.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by lirons on 24/01/2018.
 */
@Module
public abstract class CitiesModule {
    @ActivityScoped
    @Binds
    abstract CitiesContract.Presenter citiesPresenter(CitiesPresenter presenter);

}
