package com.lirons.brewery.cities;

import android.support.annotation.Nullable;

import com.lirons.brewery.data.cities.CitiesProvider;
import com.lirons.brewery.data.cities.CityInner;
import com.lirons.brewery.di.ActivityScoped;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by lirons on 24/01/2018.
 */

@ActivityScoped
public class CitiesPresenter implements CitiesContract.Presenter {
    @Inject
    public CitiesProvider cityProvider;
    private boolean parseCsvFile = true;
    private Disposable disposable;
    @Nullable
    private CitiesContract.View view;


    @Inject
    public CitiesPresenter() {
    }


    private void parseFile() {
        disposable = cityProvider.parseFile()
                .subscribeWith(new DisposableObserver<List<CityInner>>() {
                    @Override
                    public void onNext(List<CityInner> cities) {
                        parseCsvFile = false;
                        view.showCities(cities);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showLoadingCitiesError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {


                    }
                });
    }

    private void getCitiesFromDb() {
        disposable = cityProvider.getAllCities()
                .subscribeWith(new DisposableObserver<List<CityInner>>() {
                    @Override
                    public void onNext(List<CityInner> cities) {
                        view.showCities(cities);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showLoadingCitiesError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {


                    }
                });
    }


    @Override
    public void loadCities() {
        if (parseCsvFile) {
            parseFile();
        } else {
            getCitiesFromDb();
        }
    }

    @Override
    public void takeView(CitiesContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        view = null;
    }

    public void startAllQueryDataBase(String query) {
        disposable = cityProvider.getQueryCities(query).subscribeWith(new DisposableObserver<List<CityInner>>() {
            @Override
            public void onNext(List<CityInner> cities) {
                view.showCities(cities);
            }

            @Override
            public void onError(Throwable e) {
                view.showLoadingCitiesError(e.getMessage());
            }

            @Override
            public void onComplete() {
            }
        });
    }


}
