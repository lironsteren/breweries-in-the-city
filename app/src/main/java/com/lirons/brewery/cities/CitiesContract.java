package com.lirons.brewery.cities;

import com.lirons.brewery.base.BasePresenter;
import com.lirons.brewery.base.BaseView;
import com.lirons.brewery.data.cities.CityInner;

import java.util.List;

/**
 * Created by lirons on 24/01/2018.
 */

public interface CitiesContract {


    interface View extends BaseView<Presenter> {

        void showCities(List<CityInner> cities);

        void showLoadingCitiesError(String error);
    }

    interface Presenter extends BasePresenter<View> {

        void loadCities();

        void takeView(CitiesContract.View view);

        void dropView();
    }
}
