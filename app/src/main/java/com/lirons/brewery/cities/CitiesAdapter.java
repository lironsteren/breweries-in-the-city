package com.lirons.brewery.cities;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lirons.brewery.BR;
import com.lirons.brewery.R;
import com.lirons.brewery.data.cities.CityInner;
import com.lirons.brewery.utiles.RecycleClickInterface;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by lirons on 24/01/2018.
 */

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.BindingHolder> implements RecycleClickInterface {

    private List<CityInner> citiesList = new ArrayList<>();
    private CitiesAdapter.ICityClicked listener;


    public interface ICityClicked {
        void onCityClicked(CityInner city);

    }

    @Inject
    public CitiesAdapter() {
    }

    @Override
    public CitiesAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final ViewDataBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.layout_cities, parent, false);

        CitiesAdapter.BindingHolder holder = new CitiesAdapter.BindingHolder(binding.getRoot());
        holder.setBinding(binding);
        holder.setClickedListener(this);

        return holder;
    }

    @Override
    public void onBindViewHolder(CitiesAdapter.BindingHolder holder, int position) {
        CityInner city = citiesList.get(position);
        holder.getBinding().setVariable(BR.city, city);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return citiesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onItemClicked(int position) {
        if (listener != null) {
            listener.onCityClicked(citiesList.get(position));
        }
    }


    public class BindingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ViewDataBinding binding;
        private RecycleClickInterface clickedListener;


        public BindingHolder(View rowView) {
            super(rowView);
            rowView.setOnClickListener(this);

        }

        public ViewDataBinding getBinding() {
            return binding;
        }

        public void setBinding(ViewDataBinding binding) {
            this.binding = binding;
        }

        @Override
        public void onClick(View v) {
            clickedListener.onItemClicked(getAdapterPosition());
        }

        public void setClickedListener(RecycleClickInterface clickedListener) {
            this.clickedListener = clickedListener;
        }
    }


    public void refreshList(List<CityInner> cities) {
        citiesList.clear();
        citiesList.addAll(cities);
        notifyDataSetChanged();
    }

    public void setClickeListener(CitiesAdapter.ICityClicked listener) {
        this.listener = listener;
    }
}
