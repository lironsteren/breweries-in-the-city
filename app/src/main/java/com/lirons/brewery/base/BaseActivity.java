package com.lirons.brewery.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;

import com.lirons.brewery.R;

import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by lirons on 13/01/2018.
 */

public abstract class BaseActivity extends DaggerAppCompatActivity {


    public void replaceFragment(Fragment fragment, Bundle bundle, int id, String tag, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        fragmentTransaction.replace(id, fragment, tag);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }


    public void addFragment(Fragment fragment, int id, String tag, boolean addToBackStack, Bundle bundle) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        fragmentTransaction.add(id, fragment, tag);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    public void setToolBarView() {
        Toolbar toolBar = findViewById(R.id.toolBar);
        toolBar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
    }
}
