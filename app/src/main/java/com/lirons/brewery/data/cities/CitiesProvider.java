package com.lirons.brewery.data.cities;

import com.lirons.brewery.BreweryApplication;
import com.lirons.brewery.utiles.SchedulerProvider;
import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.realm.Case;
import io.realm.Realm;
import io.realm.Sort;

/**
 * Created by lirons on 24/01/2018.
 */

@Singleton
public class CitiesProvider {
    private List<CityRealm> cities;
    private List<CityInner> innerModelList;

    @Inject
    public CitiesProvider() {
    }




    public Observable<List<CityInner>> getAllCities() {

        return getAllCitiesFromDB()
                .subscribeOn(SchedulerProvider.getInstance().getIOScheduler())
                .observeOn(SchedulerProvider.getInstance().getUIScheduler());
    }

    public Observable<List<CityInner>> parseFile() {
        return parseCsvFile()
                .subscribeOn(SchedulerProvider.getInstance().getIOScheduler())
                .observeOn(SchedulerProvider.getInstance().getUIScheduler());
    }

    private Observable<List<CityInner>> getAllCitiesFromDB() {
        return Observable.fromCallable(new Callable<List<CityInner>>() {
            @Override
            public List<CityInner> call() throws Exception {
                Realm realm = Realm.getDefaultInstance();
                cities = realm.where(CityRealm.class).findAllSorted("city", Sort.ASCENDING);
                List<CityInner> innerModelList = new ArrayList<>();
                int index = 0;

                for (; index < cities.size(); index++) {
                    CityInner currItem = new CityInner(cities.get(index));
                    innerModelList.add(currItem);
                }
                realm.close();
                return innerModelList;
            }
        });
    }


    private Observable<List<CityInner>> parseCsvFile() {

        return Observable.fromCallable(new Callable<List<CityInner>>() {
            @Override
            public List<CityInner> call() throws Exception {
                final Realm realm = Realm.getDefaultInstance();
                try {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            String csvFile = "simplemaps-worldcities-basic.csv";
                            CityRealm city = new CityRealm();
                            String[] nextLine;
                            CSVReader reader = null;
                            try {
                                reader = new CSVReader(new InputStreamReader(BreweryApplication.instance().getAssets().open(csvFile)));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            int id = 0;
                            innerModelList = new ArrayList<>();
                            try {
                                while ((nextLine = reader.readNext()) != null) {
                                    city.setId(id);
                                    city.setCity(nextLine[0]);
                                    city.setCity_ascii(nextLine[1]);
                                    city.setLat(Double.parseDouble(nextLine[2]));
                                    city.setLng(Double.parseDouble(nextLine[3]));
                                    city.setPop(Double.parseDouble(nextLine[4]));
                                    city.setCountry(nextLine[5]);
                                    city.setIso2(nextLine[6]);
                                    city.setIso3(nextLine[7]);
                                    city.setProvince(nextLine[8]);
                                    id++;
                                    CityInner inner = new CityInner(city);
                                    innerModelList.add(inner);
                                    realm.insertOrUpdate(city);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } finally {
                    if (realm != null) {
                        realm.close();
                    }
                }
                return innerModelList;
            }
        }).subscribeOn(SchedulerProvider.getInstance().getIOScheduler())
                .observeOn(SchedulerProvider.getInstance().getUIScheduler());
    }



    public Observable<List<CityInner>> getQueryCities(String query) {

        return getQueryCitiesFromDb(query)
                .subscribeOn(SchedulerProvider.getInstance().getIOScheduler())
                .observeOn(SchedulerProvider.getInstance().getUIScheduler());
    }

    private Observable<List<CityInner>> getQueryCitiesFromDb(final String query) {
        return Observable.fromCallable(new Callable<List<CityInner>>() {
            @Override
            public List<CityInner> call() throws Exception {
                Realm realm = Realm.getDefaultInstance();
                cities = realm.where(CityRealm.class)
                            .contains("city", query, Case.INSENSITIVE)
                            .findAll();

                int contactIndex = 0;
                innerModelList = new ArrayList<>();
                for (; contactIndex < cities.size(); contactIndex++) {
                    CityInner currItem = new CityInner(cities.get(contactIndex));
                    innerModelList.add(currItem);
                }
                realm.close();
                return innerModelList;
            }
        });
    }

}
