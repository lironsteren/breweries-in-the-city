package com.lirons.brewery.data.breweries;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lirons on 27/01/2018.
 */

public class BreweryDetails {
    @SerializedName("id")
    private String id;
    @SerializedName("description")
    private String description;
    @SerializedName("name")
    private String name;
    @SerializedName("createDate")
    private String createDate;
    @SerializedName("updateDate")
    private String updateDate;
    @SerializedName("established")
    private long established;
    @SerializedName("isOrganic")
    private String isOrganic;
    @SerializedName("website")
    private String website;
    @SerializedName("status")
    private String status;
    @SerializedName("statusDisplay")
    private String statusDisplay;
    @SerializedName("images")
    private Images images;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public String getStatusDisplay() {
        return statusDisplay;
    }

    public void setStatusDisplay(String statusDisplay) {
        this.statusDisplay = statusDisplay;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getIsOrganic() {
        return isOrganic;
    }

    public void setIsOrganic(String isOrganic) {
        this.isOrganic = isOrganic;
    }

    public long getEstablished() {
        return established;
    }

    public void setEstablished(long established) {
        this.established = established;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}
