package com.lirons.brewery.data.breweries;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lirons on 25/01/2018.
 */

public class BreweriesResponse {
    @SerializedName("data")
    private List<Brewery> breweries;
    @SerializedName("numberOfPages")
    private int numberOfPages;
    @SerializedName("status")
    private String status ;
    @SerializedName("currentPage")
    private int currentPage;



    public List<Brewery> getBreweries() {
        return breweries;
    }
    public void setBreweries(List<Brewery> result) {
        this.breweries = result;
    }


    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }
}
