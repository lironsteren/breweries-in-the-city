package com.lirons.brewery.data.cities;

/**
 * Created by lirons on 24/01/2018.
 */

public class CityInner {

    private String city;
    private double lat;
    private double lng;
    private String country;


    public CityInner(CityRealm city) {
        this.city = city.getCity();
        this.lat = city.getLat();
        this.country = city.getCountry();
        this.lng = city.getLng();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
