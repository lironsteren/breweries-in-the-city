package com.lirons.brewery.data.breweries;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lirons on 27/01/2018.
 */

public class BreweryDetailResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private BreweryDetails brewery;
    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BreweryDetails getBrewery() {
        return brewery;
    }

    public void setBrewery(BreweryDetails brewery) {
        this.brewery = brewery;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
