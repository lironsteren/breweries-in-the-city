package com.lirons.brewery.data.breweries;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by lirons on 25/01/2018.
 */

public interface BreweriesService {
    @GET("search/geo/point")
    Observable<BreweriesResponse> getBreweriesNearLocation(@Query("key") String apiKey, @Query("lat") double lat, @Query("lng") double lng);

    @GET("brewery/{id}")
    Observable<BreweryDetailResponse> getBrewery(@Path("id") String id, @Query("key") String apiKey);
}
