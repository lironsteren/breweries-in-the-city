package com.lirons.brewery.data.breweries;

import com.lirons.brewery.utiles.SchedulerProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by lirons on 26/01/2018.
 */
@Singleton
public class BreweriesManager {

    @Inject
    public BreweriesService breweriesService;
    private static final String BREWEYDB_API_KEY = "01242420894f805bd8c76821a8ae668c";


    @Inject
    public BreweriesManager() {
    }

    public Observable<BreweriesResponse> getBreweries(double lat, double lng) {
        return breweriesService
                .getBreweriesNearLocation(BREWEYDB_API_KEY, lat, lng)
                .subscribeOn(SchedulerProvider.getInstance().getIOScheduler())
                .observeOn(SchedulerProvider.getInstance().getUIScheduler());

    }


    public Observable<BreweryDetailResponse> getBreweryDetails(String id) {
        return breweriesService
                .getBrewery(id, BREWEYDB_API_KEY)
                .subscribeOn(SchedulerProvider.getInstance().getIOScheduler())
                .observeOn(SchedulerProvider.getInstance().getUIScheduler());

    }

}

