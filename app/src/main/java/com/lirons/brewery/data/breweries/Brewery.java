package com.lirons.brewery.data.breweries;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lirons on 25/01/2018.
 */

public class Brewery {
    @SerializedName("id")
    private String id;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("name")
    private String name;
    @SerializedName("streetAddress")
    private String streetAddress;
    @SerializedName("brewery")
    private BreweryDetails brewery;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public BreweryDetails getBrewery() {
        return brewery;
    }

    public void setBrewery(BreweryDetails brewery) {
        this.brewery = brewery;
    }
}
