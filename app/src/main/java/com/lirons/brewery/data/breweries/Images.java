package com.lirons.brewery.data.breweries;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lirons on 27/01/2018.
 */

public class Images {
    @SerializedName("medium")
    private String medium;
    @SerializedName("large")
    private String large;
    @SerializedName("icon")
    private String icon;

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
