package com.lirons.brewery.utiles;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.lirons.brewery.BreweryApplication;

/**
 * Created by lirons on 24/01/2018.
 */

public class PrefStorage {
    private Context context;
    private String prefType;

    public PrefStorage(String prefType) {
        this.context = BreweryApplication.instance().getApplicationContext();
        this.prefType = prefType;
    }

    public static final class Type {
        public static final String CITY_DATA = "pref_city_data";
        public static final String KEY = "pref_realm_key";

    }

    public String get(String key) {
        return context.getSharedPreferences(prefType, Context.MODE_PRIVATE)
                .getString(key, null);
    }

    public void put(String key, String item) {
        SharedPreferences.Editor edit = context
                .getSharedPreferences(prefType, Context.MODE_PRIVATE).edit();
        edit.putString(key, item).commit();
    }

    public Boolean getBoolean(String key) {
        return context.getSharedPreferences(prefType, Context.MODE_PRIVATE)
                .getBoolean(key, false);
    }

    public void putBoolean(String key, boolean item) {
        SharedPreferences.Editor edit = context
                .getSharedPreferences(prefType, Context.MODE_PRIVATE).edit();
        edit.putBoolean(key, item).commit();
    }


    public long getLong(String key) {
        return context.getSharedPreferences(prefType, Context.MODE_PRIVATE)
                .getLong(key, 0L);
    }

    public void putLong(String key, long item) {
        SharedPreferences.Editor edit = context
                .getSharedPreferences(prefType, Context.MODE_PRIVATE).edit();
        edit.putLong(key, item).commit();
    }


    public byte[] getByte(String key) {
        String realmKey = context.getSharedPreferences(prefType, Context.MODE_PRIVATE)
                .getString(key, null);
        if (realmKey == null) {
            return null;
        }
        return Base64.decode(realmKey, Base64.DEFAULT);
    }

    public void putByte(String key, byte[] array) {

        String saveThis = Base64.encodeToString(array, Base64.DEFAULT);

        SharedPreferences.Editor edit = context
                .getSharedPreferences(prefType, Context.MODE_PRIVATE).edit();
        edit.putString(key, saveThis).commit();
    }


    public void remove(String key) {
        SharedPreferences.Editor edit = context
                .getSharedPreferences(prefType, Context.MODE_PRIVATE).edit();
        edit.remove(key).commit();
    }
}
