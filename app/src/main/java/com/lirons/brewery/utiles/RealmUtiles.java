package com.lirons.brewery.utiles;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by lirons on 24/01/2018.
 */

public class RealmUtiles {

    private static RealmConfiguration mBreweryConfiguration;
    private static PrefStorage storage;

    public static void init(Context context) {
        storage = new PrefStorage(PrefStorage.Type.KEY);

        Realm.init(context);
        mBreweryConfiguration = new RealmConfiguration.Builder()
                .name("default.realm").schemaVersion(1L)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(mBreweryConfiguration);

    }
}
