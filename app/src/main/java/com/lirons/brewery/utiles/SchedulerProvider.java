package com.lirons.brewery.utiles;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lirons on 26/01/2018.
 */

public class SchedulerProvider {
    private static SchedulerProvider sINSTANCE = null;
    private static final Object sInstanceLock = new Object();
    private Scheduler mIOScheduler;
    private Scheduler mUIScheduler;


    public static SchedulerProvider getInstance() {
        if (sINSTANCE == null) {
            synchronized (sInstanceLock) {
                if (sINSTANCE == null)
                    sINSTANCE = new SchedulerProvider();
            }
        }

        return sINSTANCE;
    }


    private SchedulerProvider() {
        mIOScheduler = Schedulers.io();
        mUIScheduler = AndroidSchedulers.mainThread();
    }


    public Scheduler getIOScheduler() {
        return mIOScheduler;
    }

    public Scheduler getUIScheduler() {
        return mUIScheduler;
    }
}
