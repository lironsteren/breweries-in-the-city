package com.lirons.brewery.utiles;

/**
 * Created by lirons on 25/01/2018.
 */

public class Constants {
    public static final String CITY_LAT = "CITY_LAT";
    public static final String CITY_LNG = "CITY_LNG";
    public static final String CITY_NAME = "CITY_NAME";
    public static final String BREWERY_ID = "BREWERY_ID";
    public static final String BREWERY_NAME = "BREWERY_NAME";


    public static final String MAP_FRAGMENT = "MAP_FRAGMENT";
    public static final String BREWERY_DETAILS_FRAGMENT = "BREWERY_DETAILS_FRAGMENT";


}
