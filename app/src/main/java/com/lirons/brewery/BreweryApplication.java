package com.lirons.brewery;

import com.lirons.brewery.di.DaggerAppComponent;
import com.lirons.brewery.utiles.RealmUtiles;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

/**
 * Created by lirons on 24/01/2018.
 */

public class BreweryApplication extends DaggerApplication {
    public static BreweryApplication mApplication;



    public static final BreweryApplication instance() {
        return mApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = this;
        RealmUtiles.init(this);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }
}
