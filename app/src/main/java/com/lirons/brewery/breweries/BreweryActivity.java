package com.lirons.brewery.breweries;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;

import com.lirons.brewery.R;
import com.lirons.brewery.base.BaseActivity;
import com.lirons.brewery.utiles.Constants;

/**
 * Created by lirons on 25/01/2018.
 */

public class BreweryActivity extends BaseActivity implements MapFragment.IBreweryClicked {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.breweries_activity);

        setToolBarView();

        Intent cityData = getIntent();
        String city = "";
        double lat = 0;
        double lng = 0;
        if (cityData != null && cityData.getExtras() != null) {
            city = cityData.getExtras().getString(Constants.CITY_NAME);
            lat = cityData.getExtras().getDouble(Constants.CITY_LAT);
            lng = cityData.getExtras().getDouble(Constants.CITY_LNG);
        }
        Bundle bundle = new Bundle();
        bundle.putString(Constants.CITY_NAME, city);
        bundle.putDouble(Constants.CITY_LAT, lat);
        bundle.putDouble(Constants.CITY_LNG, lng);

        addFragment(new MapFragment(), R.id.container, Constants.MAP_FRAGMENT, false, bundle);

    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
            if (backStackEntryCount == 0) {
                finish();
                return true;
            }
            getSupportFragmentManager().popBackStackImmediate();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openBreweryDetails(String id, String name) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.BREWERY_ID, id);
        bundle.putString(Constants.BREWERY_NAME, name);

        replaceFragment(new DetailsFragment(), bundle,R.id.container, Constants.BREWERY_DETAILS_FRAGMENT, true);
    }
}
