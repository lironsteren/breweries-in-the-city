package com.lirons.brewery.breweries;

import android.support.annotation.Nullable;

import com.lirons.brewery.data.BreweriesManagerMock;
import com.lirons.brewery.data.breweries.BreweriesManager;
import com.lirons.brewery.data.breweries.BreweriesResponse;
import com.lirons.brewery.data.breweries.BreweryDetailResponse;
import com.lirons.brewery.di.FragmentScoped;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by lirons on 25/01/2018.
 */

@FragmentScoped
public class breweryPresenter implements BreweriesContract.Presenter {

    @Nullable
    private BreweriesContract.View view;

    @Inject
    BreweriesManager breweriesManager;
    @Inject
    BreweriesManagerMock mockManager;

    private Disposable disposable;

    @Inject
    public breweryPresenter() {
    }

    @Override
    public void loadBreweries(double cityLng, double cityLat) {
        disposable = breweriesManager.getBreweries(cityLat, cityLng)
                .subscribeWith(new DisposableObserver<BreweriesResponse>() {
                    @Override
                    public void onNext(BreweriesResponse breweriesResponse) {
                        view.loadBreweriesSuccess(breweriesResponse.getBreweries());
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showLoadingError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {


                    }
                });
    }

    @Override
    public void getBreweryDetails(String id) {
        disposable = breweriesManager.getBreweryDetails(id)
                .subscribeWith(new DisposableObserver<BreweryDetailResponse>() {
                    @Override
                    public void onNext(BreweryDetailResponse breweryDetails) {
                        view.loadBreweryDetailsSuccess(breweryDetails);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showLoadingError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {


                    }
                });
    }


    @Override
    public void takeView(BreweriesContract.View view) {
        this.view = view;

    }

    @Override
    public void dropView() {
        this.view = null;
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }

    }
}
