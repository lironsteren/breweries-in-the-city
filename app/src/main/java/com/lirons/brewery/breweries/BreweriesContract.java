package com.lirons.brewery.breweries;

import com.lirons.brewery.base.BasePresenter;
import com.lirons.brewery.base.BaseView;
import com.lirons.brewery.data.breweries.Brewery;
import com.lirons.brewery.data.breweries.BreweryDetailResponse;
import com.lirons.brewery.data.breweries.BreweryDetails;

import java.util.List;

/**
 * Created by lirons on 25/01/2018.
 */

public class BreweriesContract {

    interface View extends BaseView<Presenter> {

        void loadBreweriesSuccess(List<Brewery> breweries);

        void showLoadingError(String error);

        void loadBreweryDetailsSuccess(BreweryDetailResponse details);


    }

    interface Presenter extends BasePresenter<View> {

        void loadBreweries(double cityLng, double cityLot);

        void getBreweryDetails(String id);

        void takeView(BreweriesContract.View view);

        void dropView();
    }
}
