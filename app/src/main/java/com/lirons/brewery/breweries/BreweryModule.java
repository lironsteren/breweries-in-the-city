package com.lirons.brewery.breweries;

import com.lirons.brewery.di.ActivityScoped;
import com.lirons.brewery.di.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by lirons on 25/01/2018.
 */

@Module
public abstract class BreweryModule {

    @ActivityScoped
    @FragmentScoped
    @Binds
    abstract BreweriesContract.Presenter breweryPresenter(breweryPresenter presenter);
}
