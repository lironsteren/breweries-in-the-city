package com.lirons.brewery.breweries;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.lirons.brewery.R;
import com.lirons.brewery.data.breweries.Brewery;
import com.lirons.brewery.data.breweries.BreweryDetailResponse;
import com.lirons.brewery.databinding.DetailsFragmentBinding;
import com.lirons.brewery.utiles.Constants;

import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import okhttp3.OkHttpClient;

/**
 * Created by lirons on 27/01/2018.
 */

public class DetailsFragment extends DaggerFragment implements BreweriesContract.View{

    private DetailsFragmentBinding binder;
    private String id;
    private String name;
    @Inject
    public breweryPresenter presenter;
    @Inject
    OkHttpClient client;
    private View view;

    public DetailsFragment (){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binder = DataBindingUtil.inflate(inflater, R.layout.details_fragment, container, false);
        view = binder.getRoot();
        id = getArguments().getString(Constants.BREWERY_ID);
        name = getArguments().getString(Constants.BREWERY_NAME);
        if (!TextUtils.isEmpty(id)) {
            presenter.getBreweryDetails(id);
        }
        if(!TextUtils.isEmpty(name)){
            getActivity().setTitle(name);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void loadBreweriesSuccess(List<Brewery> breweries) {

    }

    @Override
    public void showLoadingError(String error) {

    }

    @Override
    public void loadBreweryDetailsSuccess(BreweryDetailResponse details) {
        if (details.getBrewery() != null){
            if (!TextUtils.isEmpty(details.getBrewery().getDescription())){
                binder.description.setText(details.getBrewery().getDescription());
            }
            if (!TextUtils.isEmpty(details.getBrewery().getName())){
                binder.name.setText(details.getBrewery().getName());
            }

            if (!TextUtils.isEmpty(details.getBrewery().getWebsite())){
                binder.website.setText(details.getBrewery().getWebsite());
            }
            ImageView image = view.findViewById(R.id.brewery_image);
            if (details.getBrewery().getImages()!=null && !TextUtils.isEmpty(details.getBrewery().getImages().getLarge())){
                loadAvatar(image, details.getBrewery().getImages().getLarge());
            }
        }


    }

    private void loadAvatar(final ImageView imgView, String avatarUrl) {
        Glide.get(imgView.getContext())
                .register(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(client));
        Glide.with(imgView.getContext())
                .load(avatarUrl)
                .into(imgView);
    }
}
