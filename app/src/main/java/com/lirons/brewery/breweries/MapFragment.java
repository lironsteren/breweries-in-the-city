package com.lirons.brewery.breweries;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lirons.brewery.data.breweries.Brewery;
import com.lirons.brewery.data.breweries.BreweryDetailResponse;
import com.lirons.brewery.utiles.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * Created by lirons on 25/01/2018.
 */

public class MapFragment extends SupportMapFragment implements OnMapReadyCallback, HasSupportFragmentInjector, BreweriesContract.View {

    private GoogleMap mMap;
    private String cityName;
    private double cityLot;
    private double cityLng;
    private static final int MAP_DEFAULT_ZOOM_LEVEL = 12;
    @Inject
    DispatchingAndroidInjector<Fragment> childFragmentInjector;
    @Inject
    public breweryPresenter presenter;

    private List<Brewery> breweries = new ArrayList<>();
    private IBreweryClicked listener;


    interface IBreweryClicked {
        void openBreweryDetails(String id, String name);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cityName = getArguments().getString(Constants.CITY_NAME);
            cityLot = getArguments().getDouble(Constants.CITY_LAT);
            cityLng = getArguments().getDouble(Constants.CITY_LNG);
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(cityName);
        presenter.takeView(this);
        presenter.loadBreweries(cityLng, cityLot);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng city = new LatLng(cityLot, cityLng);
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                listener.openBreweryDetails(String.valueOf(marker.getTag()), marker.getTitle());
                return true;
            }
        });
        if (breweries != null) {
            for (int i = 0; i < breweries.size(); i++) {
                if (breweries.get(i).getBrewery() != null) {
                    LatLng brewery = new LatLng(breweries.get(i).getLatitude(), breweries.get(i).getLongitude());
                    Marker marker = mMap.addMarker(new MarkerOptions().position(brewery));
                    marker.setTag(breweries.get(i).getBrewery().getId());
                    marker.setTitle(breweries.get(i).getBrewery().getName());
                }

            }
        }
        mMap.setBuildingsEnabled(false);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(city, MAP_DEFAULT_ZOOM_LEVEL));


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        try {
            listener = (IBreweryClicked) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IBreweryClicked");
        }
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return childFragmentInjector;
    }

    @Override
    public void loadBreweriesSuccess(List<Brewery> breweries) {
        this.breweries = breweries;
        this.getMapAsync(this);
    }

    @Override
    public void showLoadingError(String error) {

    }

    @Override
    public void loadBreweryDetailsSuccess(BreweryDetailResponse details) {

    }
}
