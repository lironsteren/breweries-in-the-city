package com.lirons.brewery.di;

import android.app.Application;

import com.lirons.brewery.BreweryApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by lirons on 24/01/2018.
 */

@Singleton
@Component(modules = {
        ApplicationModule.class,
        ActivityBindingModule.class,
        FragmentBindingModule.class,
        ApiModule.class,
        AndroidSupportInjectionModule.class})
public interface AppComponent extends AndroidInjector<BreweryApplication> {



    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}
