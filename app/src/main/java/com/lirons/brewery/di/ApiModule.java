package com.lirons.brewery.di;

import com.lirons.brewery.data.breweries.BreweriesService;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by lirons on 25/01/2018.
 */

@Module(includes = {NetworkModule.class})
public class ApiModule {

    @Inject
    public ApiModule(){}

    @Provides
    @Singleton
    BreweriesService provideBreweriesApi(Retrofit retrofit) {
        return retrofit.create(BreweriesService.class);
    }
}
