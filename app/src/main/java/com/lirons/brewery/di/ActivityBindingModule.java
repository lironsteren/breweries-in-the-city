package com.lirons.brewery.di;

import com.lirons.brewery.breweries.BreweryActivity;
import com.lirons.brewery.breweries.BreweryModule;
import com.lirons.brewery.cities.CitiesActivity;
import com.lirons.brewery.cities.CitiesModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by lirons on 24/01/2018.
 */
@Module
public abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = CitiesModule.class)
    abstract CitiesActivity citiesActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = BreweryModule.class)
    abstract BreweryActivity breweryActivity();
}
