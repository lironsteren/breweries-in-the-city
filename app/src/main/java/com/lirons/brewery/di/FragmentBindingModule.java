package com.lirons.brewery.di;

import com.lirons.brewery.breweries.BreweryModule;
import com.lirons.brewery.breweries.DetailsFragment;
import com.lirons.brewery.breweries.MapFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by lirons on 25/01/2018.
 */

@Module
public abstract class FragmentBindingModule {
    @FragmentScoped
    @ContributesAndroidInjector(modules = BreweryModule.class)
    abstract MapFragment mapFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = BreweryModule.class)
    abstract DetailsFragment detailsFragment();

}