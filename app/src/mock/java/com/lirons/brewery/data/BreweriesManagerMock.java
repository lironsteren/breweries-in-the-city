package com.lirons.brewery.data;

import com.lirons.brewery.data.breweries.BreweriesResponse;
import com.lirons.brewery.data.breweries.Brewery;
import com.lirons.brewery.data.breweries.BreweryDetails;
import com.lirons.brewery.utiles.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by lirons on 28/01/2018.
 */
@Singleton

public class BreweriesManagerMock {

    @Inject
    public BreweriesManagerMock() {
    }
    public Observable<BreweriesResponse> getBreweries(double cityLat, double cityLng) {
        return getFakeResponse()
                .subscribeOn(SchedulerProvider.getInstance().getIOScheduler())
                .observeOn(SchedulerProvider.getInstance().getUIScheduler());

    }


    private Observable<BreweriesResponse> getFakeResponse() {
        return Observable.fromCallable(new Callable<BreweriesResponse>() {
            @Override
            public BreweriesResponse call() throws Exception {
                BreweriesResponse response = new BreweriesResponse();
                List<Brewery> list = new ArrayList<Brewery>();
                Brewery test = new Brewery();
                test.setId("OXi7r1");
                test.setLatitude(40.7562533);
                test.setLongitude(-73.9849366);
                test.setName("Heartland Brewery");
                BreweryDetails details = new BreweryDetails();
                details.setId("Nk4uV1");
                test.setBrewery(details);
                list.add(test);
                response.setBreweries(list);
                response.setCurrentPage(1);
                response.setNumberOfPages(1);
                response.setStatus("test");
                return response;
            }
        });
    }

}
